Dado('que esteja na home do site') do
    @home = Home.new
    @home.acessar_home_site
    @home.fechar_cookies
end
  
Quando('clicar em entre\/cadastre-se') do
    @home.acessar_cadastro
end
  
Então('deverá redirecionar para o modal de login') do
    expect(page).to have_content('bem-vindo!')
end