Dado('que esteja na modal de login') do
   @login = Login.new
end
  
Quando('clicar em criar uma conta') do
    @login.criar_uma_conta
end
  
Então('deverá redirecionar para a página de registro') do
    expect(current_url).to have_content('registro')
end