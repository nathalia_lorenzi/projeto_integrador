Quando ('clicar em concluir pedido') do
    @checkout = Checkout.new
    @checkout.concluir
end

E ('realizar login') do
    @checkout.login
end

E ('realizar cadastro') do
    steps %(
        Dado que esteja na modal de login
        Quando clicar em criar uma conta
        Então deverá redirecionar para a página de registro
        Dado que esteja na página de cadastro
        Quando preencher os dados cadastrais
        E aceitar os termos de uso
        E clicar em cadastrar
        Então o usuário deverá permanecer logado
    )
end

E ('seguir para o pagamento') do 
    sleep 1
    @checkout.seguir_pagamento
end

E ('selecionar a opção de frete') do
    @checkout.endereco
end

E ('selecionar a opção de pagamento boleto') do
    @checkout.boleto
end

E ('clicar em finalizar') do
    @checkout.finalizar
end

Então ('deverá redirecionar para a página do pedido') do
    @pedido_confirmado = @checkout.pedido_realizado.text
    expect(@pedido_confirmado).to have_content('PEDIDO REALIZADO')
end