Quando('clicar em remover') do
    @carrinho.remover
end

Então('o produto deverá ter sido removido') do
    expect(page).to have_content('Seu carrinho está vazio')
    sleep 2
end