Quando('filtrar por uma categoria') do
    @home.filtrar_categoria
end
  
Quando('após por subcategoria') do
    @home.filtrar_subcategoria
end
  
Então('deverão retornar produtos') do
    @busca = Busca.new
    @product_title = @busca.titulo_produto.first.text
    expect(@product_title).to include('Bota')
end