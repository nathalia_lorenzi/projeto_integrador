Dado('que esteja na página de busca com os resultados da pesquisa') do
    @busca = Busca.new
end
  
Quando('selecionar o primeiro produto') do
    @busca.selecionar_primeiro_produto
end
  
Então('deverá redirecionar para a pdp') do
    expect(current_url).to have_content('tenis-nike')
end
  
Dado('que esteja na página do primeiro produto') do
    @produto = Produto.new
end
  
Quando('selecionar o tamanho desejado') do
    sleep 3
    @produto.selecionar_tamanho_tenis
end
  
Quando('clicar em comprar') do
    @produto.comprar
end
  
Então('deverá redirecionar para o carrinho') do
    sleep 2
    expect(current_url).to have_content('carrinho')
end