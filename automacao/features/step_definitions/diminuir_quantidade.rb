Quando('clicar em diminuir a quantidade do produto') do
    @carrinho.aumentar
    @valor_produto = @carrinho.verificar_valor.text.delete('R$ ').tr(',', '.').to_f
    @carrinho.diminuir
    sleep 2
end
  
E('informações de valor da compra deve ser atualizado') do
    @valor_unitario = @carrinho.verificar_valor_unitario.text.delete('R$ ').tr(',', '.').to_f
    @valor_atual = @carrinho.verificar_valor.text.delete('R$ ').tr(',', '.').to_f
    expect(@valor_atual).to eq(@valor_produto - @valor_unitario)
    sleep 2
end 