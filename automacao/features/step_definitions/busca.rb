Quando('pesquisar pela barra de procura {string}') do |nome_busca|
    @home = Home.new
    @nome_busca = nome_busca
    @home.buscar_produto(nome_busca)
end

Então('deverá redirecionar para a página de busca') do
    @busca = Busca.new
    expect(current_url).to have_content('busca')
end
  
Então('deverão retornar produtos conforme a pesquisa') do
    @product_title = @busca.titulo_produto.first
    expect(@product_title).to have_content(@nome_busca)
end

Então('deverão retornar produtos de acordo com a pesquisa') do
    @product_title = @busca.titulo_produto.first
    expect(@product_title).to have_content(@nome_busca)
end