Dado('que esteja na página de cadastro') do
    @cadastro = Cadastro.new
end
  
Quando('preencher os dados cadastrais') do
    @cadastro.preencher_dados_pessoais
    sleep 1
    @cadastro.preencher_dados_acesso_site
    sleep 1
    @cadastro.preencher_dados_entrega
end
  
Quando('aceitar os termos de uso') do
    @cadastro.aceitar_termos
end
  
Quando('clicar em cadastrar') do
    @cadastro.cadastrar
end
  
Então('o usuário deverá permanecer logado') do
    @home = Home.new
    @home.verificar_login
end