Dado('que esteja no carrinho') do
    @carrinho = Carrinho.new
end
  
Quando('clicar em aumentar a quantidade do produto') do
    @valor_produto = @carrinho.verificar_valor.text.delete('R$ ').tr(',', '.').to_f
    @carrinho.aumentar
    sleep 2
end
  
Então('a quantidade do produto deverá ser {string}') do |quantidade|
    @quantidade_produto = @carrinho.verificar_quantidade.value
    expect(@quantidade_produto).to eq(quantidade)
end
  
E('informações de valor total da compra deve ser atualizado') do
    @valor_atual = @carrinho.verificar_valor.text.delete('R$ ').tr(',', '.').to_f
    expect(@valor_atual).to eq(@valor_produto + @valor_produto)
    sleep 2
end 