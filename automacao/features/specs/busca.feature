#language:pt

Funcionalidade: Buscar produto
    Como novo cliente
    Quero buscar produtos
    Para realizar uma compra

    @buscar_produto
    Cenário: Buscar Tênis Nike
        Dado que esteja na home do site
        Quando pesquisar pela barra de procura 'Tênis Nike'
        Então deverá redirecionar para a página de busca
        E deverão retornar produtos conforme a pesquisa