#language:pt

Funcionalidade: Cadastro/Login Cliente
    Como novo cliente
    Quero realizar o cadastro ou login
    Para adquirir produtos
    
    @redirecionamento_cadastro
    Cenário: Acessar página de cadastro
        Dado que esteja na modal de login 
        Quando clicar em criar uma conta
        Então deverá redirecionar para a página de registro
