#language:pt

Funcionalidade: Preencher Cadastro Cliente
    Como novo cliente
    Quero realizar o cadastro
    Para adquirir produtos
    
    @realizar_cadastro
    Cenário: Realizar Cadastro
        Dado que esteja na página de cadastro
        Quando preencher os dados cadastrais
        E aceitar os termos de uso
        E clicar em cadastrar
        Então o usuário deverá permanecer logado
