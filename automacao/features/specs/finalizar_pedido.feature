#language:pt

Funcionalidade: Finalizar pedido
    Como cliente
    Quero seguir com o checkout
    Para finalizar meu pedido

    @finalizar_pedido
    Cenário: Finalizar pedido
        Dado que esteja no carrinho
        Quando clicar em concluir pedido
        E realizar cadastro
        E seguir para o pagamento
        E selecionar a opção de pagamento boleto
        E clicar em finalizar
        Então deverá redirecionar para a página do pedido