#language:pt

Funcionalidade: Adicionar produto
    Como novo cliente
    Quero adicionar produtos no carrinho
    Para realizar uma compra

    @acessar_pdp
    Cenário: Acessar página do produto
        Dado que esteja na página de busca com os resultados da pesquisa
        Quando selecionar o primeiro produto
        Então deverá redirecionar para a pdp

    @adicionar_produto
    Cenário: Adicionar Tênis Nike
        Dado que esteja na página do primeiro produto
        Quando selecionar o tamanho desejado
        E clicar em comprar
        Então deverá redirecionar para o carrinho
        