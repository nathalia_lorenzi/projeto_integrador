#language:pt

Funcionalidade: Diminuir quantidade
    Como novo cliente
    Quero diminuir a quantidade de produtos no carrinho
    Para atualizar informações da compra

    @diminuir_quantidade
    Cenário: Diminuir quantidade do produto no carrinho
        Dado que esteja no carrinho 
        Quando clicar em diminuir a quantidade do produto
        Então a quantidade do produto deverá ser '1'
        E informações de valor da compra deve ser atualizado 