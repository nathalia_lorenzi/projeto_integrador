#language:pt

Funcionalidade: Remover produto
    Como novo cliente
    Quero remover produtos do carrinho
    Para atualizar carrinho

    @remover_produto
    Cenário: Remover Tênis Nike
        Dado que esteja no carrinho
        Quando clicar em remover 
        Então o produto deverá ter sido removido