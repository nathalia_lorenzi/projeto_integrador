#language:pt

Funcionalidade: Filtrar categoria/subcategoria
    Como novo cliente
    Quero realizar uma busca por categoria
    Para adquirir produtos

    @filtro_categoria
    Cenário: Filtrar categoria/subcategoria
        Dado que esteja na home do site
        Quando filtrar por uma categoria
        E após por subcategoria
        Então deverão retornar produtos