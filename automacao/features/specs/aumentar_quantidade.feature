#language:pt

Funcionalidade: Aumentar quantidade
    Como novo cliente
    Quero aumentar a quantidade de produtos no carrinho
    Para atualizar informações da compra

    @aumentar_quantidade
    Cenário: Aumentar quantidade do produto no carrinho
        Dado que esteja no carrinho 
        Quando clicar em aumentar a quantidade do produto
        Então a quantidade do produto deverá ser '2'
        E informações de valor total da compra deve ser atualizado 