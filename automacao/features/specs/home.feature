#language:pt

Funcionalidade: Cadastro Cliente
    Como novo cliente
    Quero realizar o cadastro
    Para adquirir produtos

    @redirecionamento_login
    Cenário: Redirecionamento para login/cadastro
        Dado que esteja na home do site
        Quando clicar em entre/cadastre-se
        Então deverá redirecionar para o modal de login