#language:pt

Funcionalidade: Calcular frete
    Como novo cliente
    Quero calcular entrega no carrinho
    Para realizar uma compra

    @calcular_entrega
    Cenário: Calcular entrega no carrinho
        Dado que esteja no carrinho
        Quando inserir cep
        E clicar em calcular
        Então deverá retornar o valor do frete 