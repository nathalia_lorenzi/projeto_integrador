class Home < SitePrism::Page
    include Capybara::DSL

    element  :bt_entendi, '._10i4qnf'
    element  :link_entrar, '.login_hover'
    element  :link_cadastrar, '.login_link'
    element  :inserir_busca, 'input[id="searchInput"]'
    element  :icone_pesquisa, '._1a118wb'
    element  :login_usuario, '.login_hover '
    elements :categorias, '._1od7pwn'
    elements :subcategorias, '._s8hht9'
  
    def acessar_home_site
      visit '/'
    end

    def fechar_cookies
      bt_entendi.click
    end

    def acessar_cadastro
      link_entrar.hover 
      link_cadastrar.click
    end

    def buscar_produto(nome_busca)
      inserir_busca.set(nome_busca)
      icone_pesquisa.click
    end

    def verificar_login
      login_usuario.hover
    end

    def filtrar_categoria
      categorias[1].hover
    end

    def filtrar_subcategoria
      sleep 1
      subcategorias.first.click
    end
end