class Carrinho < SitePrism::Page
    include Capybara::DSL

    element  :btn_remover, '._1kcsd3f'
    element  :btn_aumentar, '._1ah26puu'
    element  :btn_diminuir, '._1o03ozrs'
    element  :cep, '._9megmq3'
    element  :link_calcular, '._1hlurng'
    element  :quantidade, '._s09fqyu'
    elements :valor, 'p._n0vhtrx'
    element  :frete, '._slxf59'
    element  :receba_casa, '._1jrdexp'
    
    def remover
        btn_remover.click
    end

    def aumentar
        btn_aumentar.click
    end

    def verificar_quantidade
        quantidade.hover
    end

    def verificar_valor
        valor.last.hover
    end

    def verificar_valor_unitario
        valor.first.hover
    end

    def diminuir
        btn_diminuir.click
    end

    def inserir_cep
        cep.set('01153000')
    end

    def calcular_frete
        link_calcular.click
    end

    def valor_frete
        scroll_to(receba_casa)
        frete.hover
    end
end