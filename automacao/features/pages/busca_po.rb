class Busca < SitePrism::Page
    include Capybara::DSL

    elements :titulo_produto, '._e4x16a a._xe1nr1'
    elements :modal_produto, '._pqo3rp'
    element  :btn_comprar, '._1nnzvnp'

    def selecionar_primeiro_produto
        sleep 3
        titulo_produto[2].click
    end
end