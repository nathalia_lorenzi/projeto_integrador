class Login < SitePrism::Page
    include Capybara::DSL

    element  :bt_criar_conta, '._12mr5yh7'
  
    def criar_uma_conta
      bt_criar_conta.click
    end
end