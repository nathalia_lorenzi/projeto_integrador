class Checkout < SitePrism::Page
    include Capybara::DSL

    element  :btn_concluir, '._3d071f3'
    element  :email, '._g52fzx'
    element  :senha, '._t4xgys'
    element  :entrar, '._ep87247'
    element  :pagamento, '._1oj4702b'
    element  :btn_boleto, '._pdgvub'
    element  :btn_finalizar, '._1oj4702b'
    element  :pedido_realizado, '._n1wbhm'
    element  :btn_seguir_pagamento, '._1oj4702b'

    
    def concluir
        btn_concluir.click
    end

    def login
        email.set('tixac98894@luxiu2.com')
        senha.set('123456')
        entrar.click
    end

    def endereco
        scroll_to(pagamento)
        pagamento.click
    end

    def boleto
        btn_boleto.click
        
    end

    def finalizar
        scroll_to(btn_finalizar)
        btn_finalizar.click
    end

    def seguir_pagamento
        scroll_to(btn_seguir_pagamento)
        btn_seguir_pagamento.click
    end
end