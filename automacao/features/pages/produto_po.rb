class Produto < SitePrism::Page
    include Capybara::DSL

    elements :selecionar_tamanho, '._1uax8x0'
    element  :btn_comprar, '._1nnzvnp'

    def selecionar_tamanho_tenis
        sleep 1
        selecionar_tamanho.first.click
    end

    def comprar
        btn_comprar.click
    end
end