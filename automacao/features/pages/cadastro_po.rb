class Cadastro < SitePrism::Page
    include Capybara::DSL

    element  :input_name, 'input[name="Nome"]'
    element  :input_sobrenome, 'input[name="Sobrenome"]'
    element  :input_cpf, 'input[name="CPF"]'
    element  :select_sexo, 'select[name="Sexo"]'
    element  :input_dt_nascimento, '._1wja7o3 input[name="DataDeNascimento"]'
    element  :input_celular, 'input[name="TelefoneAdicional"]'
    element  :sexo_masculino, 'select[name="Sexo"] option[value="Masculino"]'
    element  :input_email, 'input[placeholder="Digite seu E-mail*"]'
    element  :input_senha, 'input[placeholder="Senha*"]'
    element  :confirmar_senha, 'input[placeholder="Confirme sua Senha*"]'
    element  :cep, 'input[name="CEP"]'
    element  :numero,  'input[name="Numero"]'
    element  :termo, '#terms-accept'
    element  :btn_cadastrar, 'input[value="Cadastrar"]'
  
    def preencher_dados_pessoais
        input_name.set(Faker::Name.first_name)
        input_sobrenome.set(Faker::Name.last_name)
        input_cpf.set(Faker::CPF.number)
        input_dt_nascimento.set('20021978')
        select_sexo.click
        sexo_masculino.click
        input_celular.set('11999887445')
    end

    def preencher_dados_acesso_site
        numero = Faker::CPF.number
        input_email.set("mariana_santos#{numero}@gmail.com")
        input_senha.set('123456&')
        confirmar_senha.set('123456&')
    end

    def preencher_dados_entrega
        cep.set('99010121')
        input_email.click
        numero.set('555')
    end

    def aceitar_termos
        termo.click
    end

    def cadastrar
        btn_cadastrar.click
    end
end
