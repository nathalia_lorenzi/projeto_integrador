Before('@redirecionamento_cadastro') do
    steps %{
        Dado que esteja na home do site
        Quando clicar em entre/cadastre-se
        Então deverá redirecionar para o modal de login
    }
end

Before('@realizar_cadastro') do
    steps %{
        Dado que esteja na home do site
        Quando clicar em entre/cadastre-se
        Então deverá redirecionar para o modal de login
        Dado que esteja na modal de login 
        Quando clicar em criar uma conta
        Então deverá redirecionar para a página de registro
    }
end

Before('@acessar_pdp') do
    steps %{
        Dado que esteja na home do site
        Quando pesquisar pela barra de procura 'Tênis Nike'
        Então deverá redirecionar para a página de busca
        E deverão retornar produtos conforme a pesquisa
    }
end

Before('@adicionar_produto') do
    steps %{
        Dado que esteja na home do site
        Quando pesquisar pela barra de procura 'Tênis Nike'
        Então deverá redirecionar para a página de busca
        E deverão retornar produtos conforme a pesquisa
        Dado que esteja na página de busca com os resultados da pesquisa
        Quando selecionar o primeiro produto
        Então deverá redirecionar para a pdp
    }
end

Before('@aumentar_quantidade') do
    steps %{
        Dado que esteja na home do site
        Quando pesquisar pela barra de procura 'Tênis Nike'
        Então deverá redirecionar para a página de busca
        E deverão retornar produtos conforme a pesquisa
        Dado que esteja na página de busca com os resultados da pesquisa
        Quando selecionar o primeiro produto
        Então deverá redirecionar para a pdp
        Dado que esteja na página do primeiro produto
        Quando selecionar o tamanho desejado
        E clicar em comprar
        Então deverá redirecionar para o carrinho
    }
end

Before('@diminuir_quantidade') do
    steps %{
        Dado que esteja na home do site
        Quando pesquisar pela barra de procura 'Tênis Nike'
        Então deverá redirecionar para a página de busca
        E deverão retornar produtos conforme a pesquisa
        Dado que esteja na página de busca com os resultados da pesquisa
        Quando selecionar o primeiro produto
        Então deverá redirecionar para a pdp
        Dado que esteja na página do primeiro produto
        Quando selecionar o tamanho desejado
        E clicar em comprar
        Então deverá redirecionar para o carrinho
    }
end

Before('@remover_produto') do
    steps %{
        Dado que esteja na home do site
        Quando pesquisar pela barra de procura 'Tênis Nike'
        Então deverá redirecionar para a página de busca
        E deverão retornar produtos conforme a pesquisa
        Dado que esteja na página de busca com os resultados da pesquisa
        Quando selecionar o primeiro produto
        Então deverá redirecionar para a pdp
        Dado que esteja na página do primeiro produto
        Quando selecionar o tamanho desejado
        E clicar em comprar
        Então deverá redirecionar para o carrinho
    }
end

Before('@calcular_entrega') do
    steps %{
        Dado que esteja na home do site
        Quando pesquisar pela barra de procura 'Tênis Nike'
        Então deverá redirecionar para a página de busca
        E deverão retornar produtos conforme a pesquisa
        Dado que esteja na página de busca com os resultados da pesquisa
        Quando selecionar o primeiro produto
        Então deverá redirecionar para a pdp
        Dado que esteja na página do primeiro produto
        Quando selecionar o tamanho desejado
        E clicar em comprar
        Então deverá redirecionar para o carrinho
    }
end

Before('@finalizar_pedido') do
    steps %{
        Dado que esteja na home do site
        Quando pesquisar pela barra de procura 'Tênis Nike'
        Então deverá redirecionar para a página de busca
        E deverão retornar produtos de acordo com a pesquisa
        Dado que esteja na página de busca com os resultados da pesquisa
        Quando selecionar o primeiro produto
        Dado que esteja na página do primeiro produto
        Quando selecionar o tamanho desejado
        E clicar em comprar
        Então deverá redirecionar para o carrinho
    }
end
