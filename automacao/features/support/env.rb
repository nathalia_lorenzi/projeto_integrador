require 'capybara'
require 'capybara/cucumber'
require 'pry'
require 'selenium-webdriver'
require 'site_prism'
require 'faker'
require 'cpf_faker'

Capybara.register_driver :selenium do |app|
    Capybara::Selenium::Driver.new(app, browser:  :chrome) 
end

# PARA RODAR NO FIREFOX (primeiro instalar geckodriver)
# Capybara.register_driver :selenium do |app|
#     Capybara::Selenium::Driver.new(app, :browser => :firefox)
# end
  
Capybara.configure do |config|
    config.run_server = false
    Capybara.default_driver = :selenium
    Capybara.page.driver.browser.manage.window.maximize 
    config.default_max_wait_time = 15
    config.app_host = 'https://www.centauro.com.br'
end
