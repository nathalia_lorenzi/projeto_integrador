# projeto_integrador

Versões utilizadas:
ChromeDriver 93.0.4577.15
Ruby: ruby 3.0.2 (mais devkit da versão)

Gems utilizadas:
capybara
cucumber
rspec
selenium-webdriver
site_prism
pry

Dependências:
Rodar o comando gem bundler install (gem que gerencia as dependências da aplicação)
Rodar o comando bundle install - Esse comando busca a Gemfile para instalar as gems do projeto

Estrutura de pastas do projeto
Features:
page: Pasta definida para armazenar as pages objects referente a automação web. O objetivo dessa pasta é armazenar as funções que serão utilizadas.
specs: pasta principal para armazer as features que por sua vez contém os bdds(cenários) das páginas web.

steps_definitions: O objetivo dessa pasta é armazenar a codificação dos steps em ruby, onde são chamados os metodos e passados os parametros necessários.

Support: Algumas pastas e arquivos foram utilizados como destacado abaixo:
env.rb: Configurações do projeto, driver, browser, url base, tamanho de tela, tempo de esperar etc...
hooks.rb: Com ele, é possível realizar um setup do cenário e uma ação no fim, como por exemplo before ou after.
pasta report: sua funcionalidade é poder armazenar os relatórios gerados durante a execução dos testes.

Configuração do do arquivo cucumber.yaml
Esse arquivo possui as configurações do formato do layout


Configuração para gerar relatório cucumber.yaml
<%time = Time.now.strftime('%d%m%Y_%H%M%S').to_s%>
A linha acima é utilizada para pegar a data e a hora do sistema, e adicionar no nome do relatório gerado durante os testes.
html_report: possui a configuração para gerar os relatórios dos testes web.
Irá armazenar o relatório com a data e a hora que foi gerado

Execução dos cenários:
Para execução de todos os cenários: cucumber
Para execução de cenário(s) especícos cucumber -t @nome_da_tag